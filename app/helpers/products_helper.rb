module ProductsHelper
	def category_sidebar
		if params[:cat_type]
      		@products = Category.find(params[:cat_type].to_i).products
    	else
    		@products = Product.all.order("name ASC")
    	end
	end
end
