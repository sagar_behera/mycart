class ProductsController < ApplicationController

  def index
    @categories = Category.order("name ASC")
    category_sidebar
  end

  def new
  	@product = Product.new
    @category = Category.new
  	@product.photos.build
  end

  def show
    @product = Product.find_by_id(params[:id])
  end

  def create
  	@product = Product.new(product_params)
  	if @product.save
      if !params[:image].blank?
        params[:image].each do |img|
          @product.photos.create(image: img)
        end
      end
  		redirect_to @product
  	else
  		render 'new'
  	end
  end

  def product_by_category
    render 'index'
  end

# Private methods
  private

  def product_params
  	params.require(:product).permit(:name, :price, :desc, :category_id,
                    :photos_attributes => [:id,:image])
  end


end
