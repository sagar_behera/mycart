class CategoriesController < ApplicationController

	respond_to :html, :js

	def create
		@category = Category.new(name: params[:name])
		if @category.save
			render json: {category: @category, status: "success"}
		else
			render json: {status: "fails"}
		end
	end

	private

	def category_params
		params.require(:category).permit(:name)
	end

	
end
