class Photo < ActiveRecord::Base
	belongs_to :product

	has_attached_file :image, 
		styles: {thumbnail: "60*60>", medium: "300*300>", large: "600*600>"},
	 	default_url: "/images/:style/missing.png"
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
	validates_attachment_presence :image
	#validates_attachment_size :image, less_than: 5megabytes

end
