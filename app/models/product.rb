class Product < ActiveRecord::Base

	#Associations
	belongs_to :category
	has_many :photos, dependent: :destroy
	#accepts_nested_attributes_for :photos,:reject_if => lambda {|t| t['image'].nil? }

	#Validations
	validates :name, presence: true 

end
