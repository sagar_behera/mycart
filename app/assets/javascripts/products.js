var ready = function(){
		$("#add-category").on('click',function(e){
			e.preventDefault();
			 $("#modal-content").modal({
				show: true
			});
		});

		$('.add-category-ajax').on('click',function(){
			var value = $('#category_name').val();
			if(value){
				$.ajax({					
				  type: 'POST',
				  url: '/categories',
				  data: {name: value},
				  success: function(data){
				  	console.log(data);
				  	$("#product_category_id").append('<option value="'+data.category.id+'">'+data.category.name+'</option>');
				  	$("#modal-content").modal('hide');
				  },	
				  error: function(error){

				  }
				});
			}
		});
};
$(document).ready(ready)
$(document).on('page:load', ready)