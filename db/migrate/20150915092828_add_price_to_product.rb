class AddPriceToProduct < ActiveRecord::Migration
  def change
  	add_column :products, :price, :decimal, precision: 10, scale: 2, default: 0.00
  end
end
